
class Alumno:
    def __init__(self, nombre, ap_materno, ap_paterno, direccion):
        self.nombre = nombre
        self.ap_materno = ap_materno
        self.ap_paterno = ap_paterno
        self.direccion = direccion

    def to_dict(self):
        return {
            'nombre': self.nombre,
            'ap_materno': self.ap_materno,
            'ap_paterno': self.ap_paterno,
            'direccion': self.direccion
        }
