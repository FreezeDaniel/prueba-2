from flask import request, jsonify
from bson.objectid import ObjectId
from models.alumno import Alumno

def init_alumno_routes(app, mongo):

    @app.route('/alumnos', methods=['POST'])
    def create_alumno():
        data = request.get_json()
        if not data:
            return jsonify({"error": "No data provided"}), 400

        nombre = data.get('nombre')
        ap_materno = data.get('ap_materno')
        ap_paterno = data.get('ap_paterno')
        direccion = data.get('direccion')

        if not all([nombre, ap_materno, ap_paterno, direccion]):
            return jsonify({"error": "Missing fields"}), 400

        alumno = Alumno(nombre, ap_materno, ap_paterno, direccion)
        alumno_id = mongo.db.alumnos.insert_one(alumno.to_dict()).inserted_id

        return jsonify({"message": "Alumno creado", "id": str(alumno_id)}), 201

    @app.route('/alumnos/<id>', methods=['GET'])
    def get_alumno(id):
        try:
            alumno = mongo.db.alumnos.find_one({"_id": ObjectId(id)})
            if not alumno:
                return jsonify({"error": "Alumno no encontrado"}), 404

            alumno['_id'] = str(alumno['_id'])
            return jsonify(alumno), 200
        except Exception as e:
            return jsonify({"error": str(e)}), 400

    @app.route('/alumnos', methods=['GET'])
    def list_alumnos():
        alumnos = mongo.db.alumnos.find()
        alumnos_list = []
        for alumno in alumnos:
            alumno['_id'] = str(alumno['_id'])
            alumnos_list.append(alumno)
        return jsonify(alumnos_list), 200

    @app.route('/alumnos/<id>', methods=['PUT'])
    def update_alumno(id):
        data = request.get_json()
        if not data:
            return jsonify({"error": "No has entregado datos"}), 400

        update_data = {}
        if 'nombre' in data:
            update_data['nombre'] = data['nombre']
        if 'ap_materno' in data:
            update_data['ap_materno'] = data['ap_materno']
        if 'ap_paterno' in data:
            update_data['ap_paterno'] = data['ap_paterno']
        if 'direccion' in data:
            update_data['direccion'] = data['direccion']

        if not update_data:
            return jsonify({"error": "sin campos que actualizar"}), 400

        result = mongo.db.alumnos.update_one(
            {"_id": ObjectId(id)},
            {"$set": update_data}
        )

        if result.matched_count == 0:
            return jsonify({"error": "Alumno no encontrado"}), 404

        return jsonify({"message": "Alumno actualizado"}), 200

    @app.route('/alumnos/<id>', methods=['DELETE'])
    def delete_alumno(id):
        result = mongo.db.alumnos.delete_one({"_id": ObjectId(id)})

        if result.deleted_count == 0:
            return jsonify({"error": "Alumno no encontrado"}), 404

        return jsonify({"message": "Alumno borrado"}), 200
